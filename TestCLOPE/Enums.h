#pragma once


enum class TransactionSourceType : unsigned int
{
	AbstractTS = 0,
	MushroomTS = 1
};

enum class AlgorihmType : unsigned int
{
	AbstarctAlg = 0,
	StringedClopeAlg = 1
};

enum class TransactionType : unsigned int
{
	AbstractTns = 0,
	TemplateTns = 1
};