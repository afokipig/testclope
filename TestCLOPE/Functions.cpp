#include "Functions.h"


namespace Func
{

	std::vector<Data<std::string>> Str2Vec(const std::string& line)
	{
		std::vector< Data<std::string> > result;

		result.reserve(line.size());

		for (size_t i{ 0 }; i < line.size(); ++i)
		{
			if (line.at(i) == '?')
			{
				continue;
			}
			result.push_back(Data<std::string>(Stringify(i) + line.at(i)));
		}

		result.shrink_to_fit();

		return result;
	}

}