#pragma once

#include "Clope.h"

#include <string>

class StringedClope : public Clope<std::string>
{
public:
	virtual AlgorihmType Type() const override
	{
		return AlgorihmType::StringedClopeAlg;
	}

};

