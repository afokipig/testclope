#pragma once

#include "Types.h"

template<class T>
class ICLuster
{
public:
	virtual ~ICLuster() = default;

	virtual void AddObject(const Transaction<T>& transact) = 0;

	virtual void RemoveObject(const Transaction<T>& transact) = 0;

	virtual void Print() const = 0;
};

