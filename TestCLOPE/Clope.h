#pragma once

#include <map>

#include "IClopeStyleAlgorithm.h"
#include "ClopeCluster.h"

template<class T>
class Clope : public IClopeStyleAlgorithm<T>
{

	std::map<size_t, ClopeCluster<T>> _Clusters;

	double _Repulsion{ 1.f };

	size_t NextId{ 0 }; // ��������� ��������� id ��� ��������

	TransactionClusterTable TransactionTable; // transactin id || cluster id

public:

	explicit Clope()
	{

	}

	explicit Clope(double repulsion)
	{
		if (repulsion < 1)
		{
			_Repulsion = 1.f;
		}
		else
		{
			_Repulsion = repulsion;

		}
	}

	virtual ~Clope()
	{

	}

	/*
	*@brief ������� �������� �������������
	*/
	virtual long double Profit() const override
	{
		long double numerator{ 0 };		// ���������
		long double denominator{ 0 };	// �����������

		for (const auto& cluster : _Clusters)
		{
			denominator += cluster.second.W();

			numerator += cluster.second.GradientCluster(_Repulsion) * denominator;
		}

		return numerator / denominator;

	}

	/*
	* @brief ���������� ����������� ������������. ������ ���� ������ 1
	*/
	virtual void SetRepulsion(double repulsion) override
	{
		if (repulsion < 1)
		{
			_Repulsion = 1.f;
		}
		else
		{
			_Repulsion = repulsion;
		}
	}

	/*
	*@brief �������� �������� ��� ������������� ���������
	*/
	virtual size_t AddInitValue(const Transaction<T>& transact) override
	{
		long double maxProfit;
		size_t maxJ;

		std::tie(maxProfit, maxJ) = CalculateProfit(transact);

		AddObjectToCluster(transact, maxJ);

		return maxJ;
	}


	/*
	*@brief �������� �������� ��� �������������
	*/
	virtual bool AddIterateValue(const Transaction<T>& transact) override
	{
		long double maxProfit;
		size_t maxJ;

		std::tie(maxProfit, maxJ) = CalculateProfit(transact);

		auto curentPos{ TransactionTable.Rows.find(transact.ID()) };

		bool bCalcRemove{ curentPos != TransactionTable.Rows.end() };

		if (bCalcRemove && maxJ != curentPos->second)
		{
			long double removeProfit{ DeltaRemove(curentPos->second, transact) };

			if (removeProfit + maxProfit < 0.f)
			{
				return false;
			}
			else
			{
				RemoveObjectFromCluster(transact, curentPos->second);

				AddObjectToCluster(transact, maxJ);

				return true;
			}
		}
		return false;
	}

	/*
	*@brief ������� ������� ������������ ���������� �� ���������
	*/
	virtual const TransactionClusterTable& GetTable() const override
	{
		return TransactionTable;
	}

	/*
	*@brief ����� � ������� ���������� � �������������
	*/
	virtual void Print() const override
	{
		size_t transactionCount{ 0 };

		for (const auto& clus : _Clusters)
		{
			clus.second.Print();
			transactionCount += clus.second.N();
		}

		std::cout << "\nTransaction count\t" << transactionCount << '\n';

		std::cout << "Cluster count\t" << _Clusters.size() << '\n';

	}


private:

	/*
	*@brief �������� ������ � �������
	*/
	virtual void AddObjectToCluster(const Transaction<T>& transact, const size_t& clusterID) override
	{
		auto clusIter{ _Clusters.find(clusterID) };

		if (clusIter != _Clusters.end())
		{
			clusIter->second.AddObject(transact);
		}
		else
		{
			ClopeCluster<T> newCluster;

			newCluster.AddObject(transact);

			_Clusters.insert({ clusterID, newCluster });

			NextId = clusterID + 1;
		}

		auto row{ TransactionTable.Rows.find(transact.ID()) };

		if (row != TransactionTable.Rows.end())
		{
			row->second = clusterID;
		}
		else
		{
			TransactionTable.InsertRow(transact.ID(), clusterID);
		}
	}

	/*
	*@brief ������� ������ �� ��������
	*/
	virtual void RemoveObjectFromCluster(const Transaction<T>& transact, const size_t& clusterID) override
	{
		auto clusIter{ _Clusters.find(clusterID) };

		if (clusIter != _Clusters.end())
		{
			clusIter->second.RemoveObject(transact);

			if (clusIter->second.N() == 0)
			{
				_Clusters.erase(clusIter);
			}
		}
	}

	/*
	*@brief ����� ����������� ������� ������� ��� ���������� ����������
	*/
	virtual std::pair<long double, size_t> CalculateProfit(const Transaction<T>& transact) override
	{
		long double currentProfit;

		long double maxProfit{ DeltaCreate(transact) };

		size_t maxJ{ NextId + 1 };

		for (const auto& cluster : _Clusters)
		{
			currentProfit = DeltaAdd(cluster.first, transact);

			if (currentProfit > maxProfit)
			{
				maxProfit = currentProfit;

				maxJ = cluster.first;
			}
		}
		return std::make_pair(maxProfit, maxJ);
	}

	/*
	*@brief ������� ��������� �������� �������������, ��� ���������� ������� � �������
	*/
	virtual long double DeltaAdd(const size_t& clusterID, const Transaction<T>& transact) const override
	{
		auto clusterIt = _Clusters.find(clusterID);

		if (clusterIt == _Clusters.end())
		{
			return NAN;
		}

		const ClopeCluster<T>& cCluster{ clusterIt->second };

		size_t newS{ cCluster.S() + transact.Size() };

		size_t newW{ cCluster.W() };

		const Data<T>* nextObject;

		for (int i{ 0 }; i < transact.Size(); ++i)
		{
			nextObject = &transact.GetObject(i);

			if (!nextObject->IsValid())
			{
				continue;
			}

			if (cCluster.Occ(*nextObject) == 0)
			{
				++newW;
			}
		}

		auto a = static_cast<long  double>(newS * (cCluster.N() + 1)) / pow(static_cast<long double>(newW), _Repulsion);

		auto b = static_cast<long  double>(cCluster.N()) * cCluster.GradientCluster(_Repulsion);

		if (std::isnan(b))
		{
			return a;
		}
		else
		{
			return a - b;
		}

	}

	/*
	*@brief ������� ��������� �������� �������������, ��� ���������� ������� � ����� �������
	*/
	virtual long double DeltaCreate(const Transaction<T>& transact) const override
	{
		size_t newS{ transact.Size() };

		size_t newW;

		newW = transact.Size();

		auto a = static_cast<long  double>(newS) / pow(static_cast<long double>(newW), _Repulsion);

		return a;
	}

	/*
	*@brief ������� ��������� ������� �������������, ��� �������� ������� �� ��������
	*/
	virtual long double DeltaRemove(const size_t& clusterID, const Transaction<T>& transact) const override
	{

		auto clusterIt = _Clusters.find(clusterID);

		if (clusterIt == _Clusters.end())
		{
			return NAN;
		}

		const ClopeCluster<T>& cCluster{ clusterIt->second };

		size_t newS{ cCluster.S() - transact.Size() };

		size_t newW{ cCluster.W() };

		const Data<T>* nextObject{ nullptr };

		for (int i{ 0 }; i < transact.Size(); ++i)
		{
			nextObject = &transact.GetObject(i);

			if (!nextObject->IsValid())
			{
				continue;
			}

			if (cCluster.Occ(*nextObject) == 0)
			{
				--newW;
			}
		}

		auto a = static_cast<long  double>(newS * (cCluster.N() + 1)) / pow(static_cast<long double>(newW), _Repulsion);

		auto b = static_cast<long  double>(cCluster.N()) * cCluster.GradientCluster(_Repulsion);

		if (std::isnan(b))
		{
			return a;
		}
		else
		{
			return a - b;
		}
	}


};

