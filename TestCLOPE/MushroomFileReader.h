#pragma once

#include "FileReader.h"

#include "Functions.h"


class MushroomFileReader : public FileReader<std::string>
{
public:

	explicit MushroomFileReader()
	{

	}

	explicit MushroomFileReader(const std::string& path)
		: FileReader<std::string>{ path }
	{

	}

	explicit MushroomFileReader(char* path)
		: FileReader<std::string>{ path }
	{

	}

	virtual ~MushroomFileReader() override
	{
		FileReader::~FileReader();
	}

	virtual std::shared_ptr<Transaction<std::string>> NextTransaction() override
	{
		if (!ReadyToNext())
		{
			return nullptr;
		}

		std::string line;

		std::getline(File, line);

		if (line.empty())
		{
			return nullptr;
		}

		line.erase(std::remove(line.begin(), line.end(), ','), line.end());
		
		std::shared_ptr<Transaction<std::string>> ptr{ new Transaction<std::string>{ LineId++, Func::Str2Vec(line) } };

		return ptr;

	}

	virtual TransactionSourceType Type() const override
	{
		return TransactionSourceType::MushroomTS;
	}
};

