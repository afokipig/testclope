#pragma once

#include "IClusteringAlgorithm.h"


template<class T>
class IClopeStyleAlgorithm : public IClusteringAlgorithm<T>
{
protected:
	/*
	*@brief ����� ����������� ������� ������� ��� ���������� ����������
	*/
	virtual std::pair<long double, size_t> CalculateProfit(const Transaction<T>& transact) = 0;

	/*
	*@brief ������� ��������� �������� �������������, ��� ���������� ������� � �������
	*/
	virtual long double DeltaAdd(const size_t& cluster, const Transaction<T>& transact) const = 0;

	/*
	*@brief ������� ��������� �������� �������������, ��� ���������� ������� � ����� �������
	*/
	virtual long double DeltaCreate(const Transaction<T>& transact) const = 0;

	/*
	*@brief ������� ��������� ������� �������������, ��� �������� ������� �� ��������
	*/
	virtual long double DeltaRemove(const size_t& clusterID, const Transaction<T>& transact) const = 0;

};

