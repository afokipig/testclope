#pragma once

#include <sstream>
#include <vector>

#include "Types.h"

namespace Func
{

	/*
	*@brief ���������� � ������
	*/
	template<class T>
	std::string Stringify(const T& val)
	{
		std::stringstream ss;
		ss << val;
		return ss.str();
	}

	/*
	*@brief ���������� ������ � ������� ���������. ��� ���������� ������ ���� ����������, ������ �������� ������� ������ ���� ���������� �� ������ ���������� ����� �� �������
	*/
	std::vector<Data<std::string>> Str2Vec(const std::string& line);


}
