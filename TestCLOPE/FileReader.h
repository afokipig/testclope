#pragma once

#include "ITransactionSource.h"

#include <string>
#include <fstream>

template<class T>
class FileReader : public ITransactionSource<T>
{
protected:

	std::string FilePath;

	size_t LineId{ 0 };

	std::ifstream File;

public:

	explicit FileReader()
	{

	}

	explicit FileReader(const std::string& path)
		: FilePath{ path }
	{

	}

	explicit FileReader(char* path)
		: FilePath{ path }
	{

	}

	virtual ~FileReader()
	{
		if (File.is_open())
		{
			File.close();
		}
	}

	void SetFilePath(const std::string& path)
	{
		FilePath = path;
	}

	void SetFilePath(char* path)
	{
		FilePath = path;
	}

	virtual bool OpenCource() override
	{
		if (File.is_open())
		{
			return true;
		}

		File.open(FilePath.c_str(), std::ios::in);

		LineId = 0;

		return File.is_open();
	}

	virtual void CloseSource() override
	{
		File.close();
	}

	virtual bool ReturnToStart()  override
	{
		if (File.is_open())
		{
			File.close();
		}

		return OpenCource();

	}

protected:

	virtual bool ReadyToNext() const override
	{
		return File.is_open();
	}

};

