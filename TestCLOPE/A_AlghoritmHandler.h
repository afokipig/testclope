#pragma once

#include <iostream>

#include "ITransactionSource.h"
#include "IClusteringAlgorithm.h"

#include "StringedClope.h"
#include "MushroomFileReader.h"


template<class T>
class A_AlghoritmHandler
{
	IClusteringAlgorithm<T>* ClusteringAlgorithm{ nullptr };

	ITransactionSource<T>* TransactionSource{ nullptr };

public:

	A_AlghoritmHandler()
	{}

	A_AlghoritmHandler(AlgorihmType algType, TransactionSourceType transType)
	{
		SetAlgorithmType(algType);
		SetTransactionSource(transType);
	}

	A_AlghoritmHandler(AlgorihmType algType, TransactionSourceType transType, const std::string& transacInitInfo)
		: A_AlghoritmHandler(algType, transType)
	{
		InitTransactionSourceWithString(transacInitInfo);
	}

	virtual ~A_AlghoritmHandler()
	{
		if (ClusteringAlgorithm)
		{
			delete ClusteringAlgorithm;
		}
		if (TransactionSource)
		{
			delete TransactionSource;
		}
	}

	bool SetAlgorithmType(AlgorihmType type)
	{
		switch (type)
		{
		case AlgorihmType::AbstarctAlg:

			std::cout << "\nAbstract algorithm is not possible\n";

			return false;

		case AlgorihmType::StringedClopeAlg:

			ClusteringAlgorithm = new StringedClope;

			return true;
		}
		return false;
	}

	void SetRepulsionByFNumber(double repulsion)
	{
		ClusteringAlgorithm->SetRepulsion(repulsion);
	}

	bool SetTransactionSource(TransactionSourceType type)
	{
		switch (type)
		{
		case TransactionSourceType::AbstractTS:

			std::cout << "\nAbstract transaction source is not possible\n";

			return false;

		case TransactionSourceType::MushroomTS:

			TransactionSource = new MushroomFileReader;

			return true;
		}
		return false;
	}

	bool InitTransactionSourceWithString(const std::string& openSettings)
	{
		auto type = TransactionSource->Type();

		switch (type)
		{
		case TransactionSourceType::AbstractTS:

			return false;

		case TransactionSourceType::MushroomTS:

			MushroomFileReader* ptr{ dynamic_cast<MushroomFileReader*>(TransactionSource) };

			ptr->SetFilePath(openSettings);

			return ptr->OpenCource();

		}
		return false;
	}

	size_t InitAlgorithm()
	{

		std::shared_ptr <Transaction<T>> transact{ TransactionSource->NextTransaction() };

		size_t count{ 0 };

		while (transact && transact->IsValid())
		{
			++count;

			ClusteringAlgorithm->AddInitValue(*transact);

			transact = TransactionSource->NextTransaction();
		}

		TransactionSource->ReturnToStart();

		return count;
	}

	bool IterateAlgorithm()
	{
		std::shared_ptr <Transaction<T>> transact{ TransactionSource->NextTransaction() };

		bool bMoved{ false };

		while (transact && transact->IsValid())
		{
			if (ClusteringAlgorithm->AddIterateValue(*transact))
			{
				bMoved = true;
			}

			transact = TransactionSource->NextTransaction();
		}

		TransactionSource->ReturnToStart();

		return bMoved;
	}

	void InitAndIterateAlgorithm()
	{
		InitAlgorithm();

		bool bMoved{ true };

		while (bMoved)
		{
			bMoved = IterateAlgorithm();
		}
	}

	void PrintInfo()
	{
		ClusteringAlgorithm->Print();
	}
};

