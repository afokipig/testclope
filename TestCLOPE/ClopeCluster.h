#pragma once

#include "ICLuster.h"

#include <set>

template <class T>
class ClopeCluster : public ICLuster<T>
{
	std::map<Data<T>, size_t> _Objects;

	size_t _S{ 0 };

	long double _H{ 0.f };

	size_t _N{ 0 };

public:

	virtual ~ClopeCluster() override
	{

	}

	/*
	*@brief ������ ��������
	*/
	size_t S() const
	{
		return _S;
	}

	/*
	*@brief ������ ��������
	*/
	long double H() const
	{
		return _H;
	}

	/*
	*@brief ���������� ���������� ��������� � �������� / ������ ��������
	*/
	size_t W() const
	{
		return _Objects.size();
	}

	/*
	*@brief ���������� ���������� � ��������
	*/
	size_t N() const
	{
		return _N;
	}

	/*
	*@brief ���������� ���������� � �������
	*/
	virtual void AddObject(const Transaction<T>& transact) override
	{
		_S += transact.Size();

		const Data<T>* nextObject;

		for (size_t i{ 0 }; i < transact.Size(); ++i)
		{
			nextObject = &transact.GetObject(i);

			if (!nextObject->IsValid())
			{
				continue;
			}

			const auto objIterator{ _Objects.find(*nextObject) };

			if (objIterator != _Objects.end())
			{
				++objIterator->second;
			}
			else
			{
				_Objects.insert({ *nextObject, 1 });
			}
		}

		_H = static_cast<long double>(_S) / static_cast<long double>(W());

		++_N;
	}

	/*
	*@brief �������� ���������� �� ��������
	*/
	virtual void RemoveObject(const Transaction<T>& transact) override
	{
		_S -= transact.Size();

		const Data<T>* nextObject;

		for (size_t i{ 0 }; i < transact.Size(); ++i)
		{
			nextObject = &transact.GetObject(i);
			
			if (!nextObject->IsValid())
			{
				continue;
			}
			const auto objIterator{ _Objects.find(*nextObject) };

			if (objIterator != _Objects.end())
			{
				--objIterator->second;

				if (objIterator->second == 0)
				{
					_Objects.erase(objIterator);
				}
			}
		}

		if (_S == 0 || W() == 0)
		{
			_H = 0.f;
		}
		else
		{
			_H = static_cast<long double>(_S) / static_cast<long double>(W());
		}
		--_N;
	}

	/*
	*@brief ���������� ��������� ������� � �������
	*/
	size_t Occ(const Data<T>& object) const
	{
		const auto result{ _Objects.find(object) };

		if (result != _Objects.end())
		{
			return result->second;
		}
		return 0;
	}

	/*
	*@brief ����� ���������� � �������� � �������
	*/
	virtual void Print() const override
	{
		std::cout << "\n\n" << "S\t" << _S << "\nH\t" << _H << "\nW\t" << W() << "\nN\t" << _N << '\n';
		for (const std::pair<Data<T>, size_t>& ob : _Objects)
		{
			std::cout << "object\t" << ob.first.GetData() << "\t value \t" << ob.second << '\n';
		}
		std::cout << "\n\n";
	}

	/*
	*@brief �������� ��������
	*/
	long double GradientCluster(double repulsion) const
	{
		if (repulsion < 1.f)
		{
			repulsion = 1.f;
		}

		return static_cast<long  double>(_S) / pow(W(), repulsion);
	}
};

