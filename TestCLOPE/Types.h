#pragma once

#include <vector>
#include <map>
#include <list>

#include "Enums.h"


template<class T>
class Data
{
	std::pair<bool, T> _Data;

public:

	explicit Data()
		: _Data{ std::make_pair(false, T()) }
	{

	}

	explicit Data(const T& data)
		: _Data{ std::make_pair(true, data) }
	{

	}

	virtual ~Data() = default;

	virtual const T& GetData() const
	{
		return _Data.second;
	}

	virtual bool IsValid() const
	{
		return _Data.first;
	}

	virtual bool operator<(const Data<T>& other)
	{
		return _Data.second < other._Data.second;
	}

	virtual bool operator>(const Data<T>& other)
	{
		return _Data.second > other._Data.second;
	}

	virtual bool operator==(const Data<T>& other)
	{
		return _Data.second == other._Data.second;
	}

	virtual bool operator!=(const Data<T>& other)
	{
		return _Data.second != other._Data.second;
	}
};

template<class T>
bool operator<(const Data<T>& their, const Data<T>& other)
{
	return their.GetData() < other.GetData();
}

template<class T>
bool operator>(const Data<T>& their, const Data<T>& other)
{
	return their.GetData() > other.GetData();
}

template<class T>
bool operator==(const Data<T>& their, const Data<T>& other)
{
	return their.GetData() == other.GetData();
}

template<class T>
bool operator!=(const Data<T>& their, const Data<T>& other)
{
	return their.GetData() != other.GetData();
}

template<class T>
class Transaction
{
	const size_t _ID{ 0 };

	const std::vector<Data<T>> Objects{};

	const bool bValidTransaction;

public:

	explicit Transaction()
		: bValidTransaction{ false }
	{}

	explicit Transaction(const size_t& id, const std::vector<Data<T>>& obj)
		: Objects{ obj }
		, _ID{ id }
		, bValidTransaction{ true }
	{}
	
	explicit Transaction(const size_t& id, std::vector<Data<T>>&& obj)
		: Objects{ obj }
		, _ID{ id }
		, bValidTransaction{ true }
	{}

	virtual size_t ID() const
	{
		return _ID;
	}

	virtual bool IsValid() const
	{
		return bValidTransaction;
	}

	virtual TransactionType GetType() const
	{
		return TransactionType::TemplateTns;
	}

	virtual size_t Size() const
	{
		return Objects.size();
	}

	virtual const Data<T>& GetObject(const size_t& pos) const
	{
		if (pos > Objects.size())
		{
			return std::move(Data<T>());
		}
		else
		{
			return Objects.at(pos);
		}
	}
};



class TransactionClusterTable
{
public:

	std::map<size_t, size_t> Rows;

	explicit TransactionClusterTable();

	explicit TransactionClusterTable(const std::map<size_t, size_t>& rows);

	explicit TransactionClusterTable(std::map<size_t, size_t>&& rows);

	void InsertRow(const size_t& transactID, const size_t& clusId);

};
