#pragma once

#include "Types.h"
#include "Enums.h"
#include "ICluster.h"

template<class T>
class IClusteringAlgorithm
{

public:

	/*
	*@brief ������� �������� �������������
	*/
	virtual long double Profit() const = 0;

	/*
	* @brief ���������� ����������� ������������. ������ ���� ������ 1
	*/
	virtual void SetRepulsion(double repulsion) = 0;

	/*
	*@brief �������� �������� ��� ������������� ���������
	*/
	virtual size_t AddInitValue(const Transaction<T>& transact) = 0;

	/*
	*@brief �������� �������� ��� �������������
	*/
	virtual bool AddIterateValue(const Transaction<T>& transact) = 0;

	/*
	*@brief ������� ������� ������������ ���������� �� ���������
	*/
	virtual const TransactionClusterTable& GetTable() const = 0;

	/*
	*@brief ����� � ������� ���������� � �������������
	*/
	virtual void Print() const = 0;

	virtual AlgorihmType Type() const = 0;

protected:

	/*
	*@brief �������� ������ � �������
	*/
	virtual void AddObjectToCluster(const Transaction<T>& line, const size_t& clusterID) = 0;

	/*
	*@brief ������� ������ �� ��������
	*/
	virtual void RemoveObjectFromCluster(const Transaction<T>& line, const size_t& clusterID) = 0;

};

