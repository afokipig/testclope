#include "Types.h"



TransactionClusterTable::TransactionClusterTable()
{

}

TransactionClusterTable::TransactionClusterTable(const std::map<size_t, size_t>& rows)
	: Rows{ rows }
{

}

TransactionClusterTable::TransactionClusterTable(std::map<size_t, size_t>&& rows)
	: Rows{ rows }
{

}

void TransactionClusterTable::InsertRow(const size_t& transactID, const size_t& clusId)
{
	auto row{ Rows.find(transactID) };

	if (row == Rows.end())
	{
		Rows.insert({ transactID, clusId });
	}
	else
	{
		row->second = clusId;
	}

}

