#pragma once

#include "Types.h"
#include "Enums.h"


template<class T>
class ITransactionSource
{

public:

	virtual bool OpenCource() = 0;

	virtual void CloseSource() = 0;

	virtual std::shared_ptr<Transaction<T>> NextTransaction() = 0;

	virtual bool ReturnToStart() = 0;

	virtual TransactionSourceType Type() const = 0;

protected: 

	virtual bool ReadyToNext() const = 0;

};

